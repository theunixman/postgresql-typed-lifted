# PostgreSQL Typed: Lifted

This provides lifted operations
from
[PostgreSQL Typed](http://hackage.haskell.org/package/postgresql-typed). The
operations are lifted
into
[MonadBaseControl](https://hackage.haskell.org/package/monad-control),
and exceptions are lifted
with [Exceptions](https://hackage.haskell.org/package/exceptions).

## Versioning

The version numbers we use track [PostgreSQL Typed](http://hackage.haskell.org/package/postgresql-typed).
