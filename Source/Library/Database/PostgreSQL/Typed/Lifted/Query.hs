{-|
Module:             Database.PostgreSQL.Typed.Lifted.Query
Description:        Lifted versions of Database.PostgreSQL.Typed.Query
Copyright:          © 2017 All rights reserved.
License:            BSD3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Database.PostgreSQL.Typed.Lifted.Query (
    module Database.PostgreSQL.Typed.Query,
    pgExecute,
    pgQuery,
    pgLazyQuery
    ) where

import Control.Monad.Base
import Control.Monad.Trans.Control
import Database.PostgreSQL.Typed.Query hiding (pgExecute, pgQuery, pgLazyQuery)
import Database.PostgreSQL.Typed.Lifted.Protocol
import qualified Database.PostgreSQL.Typed.Query as Q

pgExecute ∷ (MonadBaseControl IO m, PGQuery q ()) ⇒
    PGConnection → q → m Int
pgExecute c q = liftBase $ Q.pgExecute c q

pgQuery ∷ (MonadBaseControl IO m, PGQuery q a) ⇒
    PGConnection → q → m [a]
pgQuery c q = liftBase $ Q.pgQuery c q

pgLazyQuery ∷ (MonadBaseControl IO m) ⇒
    PGConnection → PGPreparedQuery a → ChunkRows → m [a]
pgLazyQuery c q r = liftBase $ Q.pgLazyQuery c q (fromIntegral r)
