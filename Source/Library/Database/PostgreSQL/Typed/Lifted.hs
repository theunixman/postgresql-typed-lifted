{-|
Module:             Database.PostgreSQL.Typed.Lifted
Description:        Lifted versions of PostgreSQL Typed.
Copyright:          © 2017 All rights reserved.
License:            BSD3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Database.PostgreSQL.Typed.Lifted (
    module Database.PostgreSQL.Typed.Lifted.Protocol
    ) where

import Database.PostgreSQL.Typed.Lifted.Protocol
